<?php

namespace daniel\mydemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="mail")
 */
class Mail
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $title;

    /**
     * @ORM\Column(type="text")
     */
    protected $body;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $absender;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $empfaenger;


    /**
     * @ORM\Column(type="datetime")
     */
    protected $time;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set to
     *
     * @param string $to
     * @return Mail
     */
    public function setEmpfaenger($empfaenger)
    {
        $this->empfaenger = $empfaenger;

        return $this;
    }

    /**
     * Get to
     *
     * @return string
     */
    public function getEmpfaenger()
    {
        return $this->empfaenger;
    }

    /**
     * Set from
     *
     * @param string $from
     * @return Mail
     */
    public function setAbsender($absender)
    {
        $this->absender = $absender;

        return $this;
    }

    /**
     * Get from
     *
     * @return string
     */
    public function getAbsender()
    {
        return $this->absender;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return Mail
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Mail
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Mail
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }
}