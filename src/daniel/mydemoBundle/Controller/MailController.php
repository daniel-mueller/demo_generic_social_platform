<?php

namespace daniel\mydemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form;
use daniel\mydemoBundle\Entity\Mail;
use daniel\mydemoBundle\Form\MailType;

class MailController extends Controller
{

    public function newAction()
    {
        $mail = new Mail();
        $form = $this->createForm(new MailType(), $mail);

        return $this->render('danielmydemoBundle:Mail:form.html.twig', array(
            'mail' => $mail,
            'form' => $form->createView()
        ));
    }

    public function sendAction()
    {

        $mail = new Mail();
        $user = $this->getUser()->getUsername();
        $mail->setAbsender($user);
        $mail->setTime(new \DateTime());
        $request = $this->getRequest();
        $form = $this->createForm(new MailType(), $mail);
        $form->bind($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($mail);
            $em->flush();

            return $this->redirect($this->generateUrl('danielmydemo_homepage'));
        }

        return $this->render('danielmydemoBundle:Mail:sendmail.html.twig', array(
            'mail' => $mail,
            'form' => $form
        ));


    }

    public function showAction()
    {
        $em = $this->getDoctrine()
            ->getManager();

        $mails = $em->createQueryBuilder()
            ->select('b')
            ->from('danielmydemoBundle:Mail', 'b')
            ->addOrderBy('b.time', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('danielmydemoBundle:Mail:mailbox.html.twig', array('mails' => $mails));
    }

}