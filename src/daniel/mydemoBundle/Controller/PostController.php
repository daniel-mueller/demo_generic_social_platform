<?php

namespace daniel\mydemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form;
use daniel\mydemoBundle\Entity\Post;
use daniel\mydemoBundle\Form\PostType;


class PostController extends Controller {

    public function newAction() {
        $post = new Post();
        $form = $this->createForm(new PostType(), $post);

        return $this->render('danielmydemoBundle:Post:form.html.twig', array(
           'post' => $post,
           'form' => $form->createView()
        ));
    }



    public function postAction()
    {
        $post = new Post();
        $user = $this->getUser()->getUsername();
        $post->setUser($user);
        $post->setTime(new \DateTime());
        $request = $this->getRequest();
        $form = $this->createForm(new PostType(), $post);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirect($this->generateUrl('danielmydemo_homepage'));
        }

        return $this->render('danielmydemoBundle:Post:makepost.html.twig', array(
           'post' => $post,
           'form' => $form->createView()
        ));

    }

    public function deleteAction($id) {

        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('danielmydemoBundle:Post')->find($id);
        $em->remove($post);
        $em->flush();

        return $this->redirect($this->generateUrl('danielmydemo_homepage'));

    }





}