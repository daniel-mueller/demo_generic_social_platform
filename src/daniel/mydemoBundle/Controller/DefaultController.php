<?php

namespace daniel\mydemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()
                    ->getManager();

        $posts = $em->createQueryBuilder()
                    ->select('b')
                    ->from('danielmydemoBundle:Post', 'b')
                    ->addOrderBy('b.time', 'DESC')
                    ->getQuery()
                    ->getResult();

        return $this->render('danielmydemoBundle:Default:index.html.twig', array('posts' => $posts));
    }
}
