<?php

namespace daniel\mydemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form;
use daniel\mydemoBundle\Entity\Post;
use daniel\mydemoBundle\Form\PostType;


class UserController extends Controller {

    public function userAction($user) {


        return $this->render('danielmydemoBundle:User:user.html.twig', array(
            'user' => $user
        ));
    }

    public function ownAction() {

        $em = $this->getDoctrine()
            ->getEntityManager();

        $posts = $em->createQueryBuilder()
            ->select('b')
            ->from('danielmydemoBundle:Post', 'b')
            ->addOrderBy('b.time', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('danielmydemoBundle:User:own.html.twig', array('posts' => $posts));
    }

}
