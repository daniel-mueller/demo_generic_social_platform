<?php
namespace daniel\mydemoBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\DateTime;
use daniel\mydemoBundle\Entity\Post;

class PostFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager) {

        $post1 = new Post();
        $post1->setUser('kalle');
        $post1->setTime(new \DateTime("2013-07-23 06:12:33"));
        $post1->setTitle('Die Jahreszeiten');
        $post1->setBody('Den Sommer hab ich am liebsten.');
        $manager->persist($post1);

        $post2 = new Post();
        $post2->setUser('sepp');
        $post2->setTime(new \DateTime("2013-07-25 06:12:33"));
        $post2->setTitle('Kaffee');
        $post2->setBody('Wenn ich mich richtig konzentrieren will trink ich erstmal ne Kanne Kaffee.');
        $manager->persist($post2);

        $post3 = new Post();
        $post3->setUser('hannes');
        $post3->setTime(new \DateTime("2013-08-01 06:12:33"));
        $post3->setTitle('Hallo');
        $post3->setBody('Hallo alle miteinander!');
        $manager->persist($post3);

        $post4 = new Post();
        $post4->setUser('hannes');
        $post4->setTime(new \DateTime("2013-08-02 06:12:33"));
        $post4->setTitle('William Blake');
        $post4->setBody('“If the doors of perception were cleansed every thing would appear to man as it is, Infinite. For man has closed himself up, till he sees all things thro narrow chinks of his cavern.”');
        $manager->persist($post4);

        $post5 = new Post();
        $post5->setUser('kalle');
        $post5->setTime(new \DateTime("2013-08-03 06:12:33"));
        $post5->setTitle('Bananen');
        $post5->setBody('Bananen sind ein ausgezeichneter Kaliumlieferant.');
        $manager->persist($post5);

        $post6 = new Post();
        $post6->setUser('sepp');
        $post6->setTime(new \DateTime("2013-08-04 06:12:33"));
        $post6->setTitle('Bären');
        $post6->setBody('Man kann einen Grizzlybär daran identifizieren, dass er an seiner Pfote 5 Krallen hat. Der Braunbär hat nur 4.');
        $manager->persist($post6);

        $manager->flush();

    }
}