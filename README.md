This is a simple mock-up of a social networking platform I built as a demo.

Features include:

- Login
- Deleting own posts as user or all as admin
- Posting to a newsfeed as a user
- Looking up posts by a specific user
- Simple system for Private Messages

